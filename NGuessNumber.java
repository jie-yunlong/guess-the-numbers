
import java.util.Random;
import java.util.Scanner;

public class NGuessNumber {
    int[] Nums = new int[4];
    int[] inputNumsArray = new int[4];
    int difficultyLevel;
    int difficulty;
    int aA = 0;
    int bB = 0;
    String numberStr = "";
    String str = "";

    public int[] randNums(int n) {
        for (int i = 0; i < Nums.length; i++) {
            Random ran = new Random();
            int a = ran.nextInt(10);
            if (i - 1 != -1) {
                for (int j = 0; j < i; j++) {
                    if (a == Nums[j]) {
                        i--;
                        break;
                    } else {
                        Nums[i] = a;
                    }
                }
            } else {
                Nums[i] = a;
            }
        }
        return Nums;
    }


    public int selectLevel() {

        @SuppressWarnings("resource")
        Scanner scan = new Scanner(System.in);
        System.out.println("答案是:" + numberStr);
        System.out.println("请选择难度系数(输入数字)，1：简单 可以猜12次;2：中等 可以猜9次;3：困难 可以猜7次");
        difficulty = scan.nextInt();
        switch (difficulty) {
            case 1:
                difficultyLevel = 10;
                break;
            case 2:
                difficultyLevel = 7;
                break;
            case 3:
                difficultyLevel = 5;
                break;
            default:
                break;
        }
        return difficultyLevel;
    }


    public int[] inputNums(int n) {
        @SuppressWarnings("resource")
        Scanner scan = new Scanner(System.in);
        int b = scan.nextInt();
        for (int i = 0; i < inputNumsArray.length; i++) {
            int c = (int) ((int) b / Math.pow(10, 3 - i));
            inputNumsArray[i] = c;
            b = (int) (b - c * Math.pow(10, (3 - i)));
        }
        return inputNumsArray;
    }


    public String compare(int[] answer, int[] inputs) {
        for (int i = 0; i < answer.length; i++) {
            if (inputs[i] == answer[i]) {
                aA += 1;
                continue;
            } else {
                for (int j = 0; j < answer.length; j++) {
                    if (inputs[i] == answer[j]) {
                        bB += 1;
                    }
                }
            }
        }
        str = "" + aA + "A " + bB + "B ";
        return str;
    }


    public void play() {
        randNums(4);
        for (int i = 0; i < Nums.length; i++) {
            numberStr = numberStr + Nums[i];
        }
        selectLevel();
        System.out.println("你选择了难度系数:" + difficulty + " 共有:" + difficultyLevel
                + "次机会。");
        for (int i = 0; i < difficultyLevel; i++) {
            inputNums(4);
            int chanceNums = difficultyLevel - i - 1;
            compare(Nums, inputNumsArray);
            if (aA != 4) {
                if (chanceNums == 0) {
                    System.out.println("机会用完了,答案是:" + numberStr);
                    break;
                } else {
                    System.out.println(str + " 你还有" + chanceNums + "次机会");
                }
                aA = 0;
                bB = 0;
            } else if (aA == 4) {
                System.out.println("恭喜你，答对了");
                break;
            }
        }
    }

    public static void main(String[] args) {
        NGuessNumber a = new NGuessNumber();
        a.play();
    }
}